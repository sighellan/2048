CXXFLAGS = -g -Wall -Wextra
CXX = g++

all: 2048

2048: 2048.cpp
	$(CXX) $(CXXFLAGS) 2048.cpp -o 2048

clean:
	rm -f 2048
