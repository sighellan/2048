/* This game is based on the program 2048 by Gabriele Ciruli: 
 * https://github.com/gabrielecirulli/2048
 *
 * This game was written as an assignment at Imperial College London in
 * February and March 2015.
 *
 * Sigrid Passano Hellan
 * CID: 00951657
 */
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>

#define RANK2 3

using namespace std;

struct rank{
    string name;
    int value;
};

// This function prints the board and the score.
void board_print(const vector<int>& status, int score);

// This function adds a 2 to a random empty space on the board.
void add_rand(vector<int>& board);

//This function checks that there are still legal moves left, and that the user hasn't decided to quit. 
bool game(const vector<int>& board);

// This function takes the terminal input and activates the appropriate function. It returns true if a change was made.
bool input_evaluation(vector<int>& board, char key, int& score);

// This function prints a message the first time a 2048 tile appears on the board.
bool winner(const vector<int>& board, bool& win_condition);

// These functions implement the move chosen by the user.
bool w(vector<int>& board, int& score);
bool a(vector<int>& board, int& score);
bool s(vector<int>& board, int& score);
bool d(vector<int>& board, int& score);
//

// These functions are used by the four functions above to do the moving of the tiles. 
void move_w(vector<int>& board, bool& test);
void move_a(vector<int>& board, bool& test);
void move_s(vector<int>& board, bool& test);
void move_d(vector<int>& board, bool& test);
//

int main(){
    //---------------------------------------------------------------
    // This part of the main sets up the start configuration.
    ifstream start_values, high_scores_input;
    string file_name;
    cout << "enter initial configuration file name: " << endl;
    cin >> file_name;
    start_values.open(file_name.c_str());
    high_scores_input.open("high_scores.txt");

    int tmp, i;
    rank in_rank;
    vector<int> board;
    vector<rank> high_scores;
    if(start_values.is_open()){
        while(start_values >> tmp){
            board.push_back(tmp);
        }
        start_values.close();
    }
    else{
        cout << "file not found, using default start configuration" << endl;
        for(i = 0; i < 15; i++){
            board.push_back(0);
        }
        board.push_back(2);
    }
    if(high_scores_input.is_open()){
        while(high_scores_input >> in_rank.name >> in_rank.value){
            high_scores.push_back(in_rank);
        }
        high_scores_input.close();
    }
    else{
        in_rank.name = "-";
        in_rank.value = 0;
        for(i = 0; i < (10 + RANK2); i++){
            high_scores.push_back(in_rank);
        }
    }
    //---------------------------------------------------------------


    //---------------------------------------------------------------
    // This part of the main controls the gameplay.
    srand (time(NULL));
    char key;
    bool in, play_condition = true, win_condition = false;
    int score = 0;
    play_condition = game(board);
    while(play_condition == true){
        board_print(board, score);
        if(winner(board, win_condition) == true){
            cout << "congratulations, you have beaten the game!" << endl;
            cout << "feel free to continue" << endl << endl;
        }
        in = false;
        while(in == false){
            cin >> key;
            in = input_evaluation(board, key, score);
        }
        if(key != 'q') add_rand(board);
        play_condition = game(board);
        cout << endl;
    }

    board_print(board, score);
    
    //---------------------------------------------------------------
    // This part of the main finishes the game.
    cout << "game over" << endl;
    int high = 0, j;
    string player = "";
    for(i = 0; i < 16; i++){
        if(board[i] > high){
            high = board[i];
        }
    }
    for(i = 0; i < (high_scores.size() - RANK2); i++){
        if(score > high_scores[i].value){
            for(j = high_scores.size()-1-RANK2; j > i; j--){
                high_scores[j] = high_scores[j-1];
            }
            cout << "please enter name for high score list: " << endl;
            high_scores[i].value = score;
            cin >> player;
            high_scores[i].name = player;
            i = high_scores.size();
        }
    }
    for(i = high_scores.size()-RANK2; i < (high_scores.size()); i++){
        if(high > high_scores[i].value){
            for(j = high_scores.size()-1; j > i; j--){
                high_scores[j] = high_scores[j-1];
            }
            if(player == ""){
                cout << "please enter name for high score list: " << endl;
                cin >> high_scores[i].name;
            }
            else high_scores[i].name = player;
            high_scores[i].value = high;
            i = high_scores.size();
        }
    }

    ofstream high_scores_output;
    high_scores_output.open("high_scores.txt");
    for(i = 0; i < high_scores.size(); i++){
        high_scores_output << high_scores[i].name << " " << high_scores[i].value << endl;
    }
    high_scores_output.close();

    cout << "highest number reached: " << high << endl;
    cout << "highest scores: " << endl;
    for(i = 0; i < high_scores.size()-RANK2; i++){
        cout << high_scores[i].name << "\t" << high_scores[i].value << endl;
    }
    cout << endl << "highest values: " << endl;
    for(i = high_scores.size()-RANK2; i < high_scores.size(); i++){
        cout << high_scores[i].name << "\t" << high_scores[i].value << endl;
    }

    return 0;
}

void board_print(const vector<int>& status, int score){
    int i;
    for(i = 0; i < 16; i++){
        if((i > 0) && (i % 4 == 0)){
            cout << endl;
        }
        cout << status[i] << "\t";
    }
    cout << endl;
    cout << "score: " << score << endl << endl;
}

bool w(vector<int>& board, int& score){
    int i;
    bool test = false;
    move_w(board, test);
    for(i = 0; i < 12; i++){
        if((board[i] == board[i+4]) && (board[i] != 0)){
            board[i] = 2*board[i];
            board[i+4] = 0;
            score = score + board[i];
            test = true;
        }
    }
    move_w(board, test);
    return test;
}

void move_w(vector<int>& board, bool& test){
    int i, j;
    for(j = 0; j < 3; j++){
        for(i = 0; i < 12; i++){
            if((board[i] == 0) && board[i+4] != 0){
                board[i] = board[i+4];
                board[i+4] = 0;
                test = true;
            }
        }
    }
}

bool s(vector<int>& board, int& score){
    int i;
    bool test = false;
    move_s(board, test);
    for(i = 15; i > 3; i--){
        if((board[i] == board[i-4]) && (board[i] != 0)){
            board[i] = 2*board[i];
            board[i-4] = 0;
            score = score + board[i];
            test = true;
        }
    }
    move_s(board, test);
    return test;
}

void move_s(vector<int>& board, bool& test){
    int i, j;
    for(j = 0; j < 3; j++){
        for(i = 15; i > 3; i--){
            if((board[i] == 0) && (board[i-4] != 0)){
                board[i] = board[i-4];
                board[i-4] = 0;
                test = true;
            }
        }
    }
}

bool a(vector<int>& board, int& score){
    int i;
    bool test = false;
    move_a(board, test);
    for(i = 0; i < 15; i++){
        if((i+1) % 4 == 0){
            i++;
        }
        if((board[i] == board[i+1]) && (board[i] != 0)){
            board[i] = 2*board[i];
            board[i+1] = 0;
            test = true;
            score = score + board[i];
        }
    }
    move_a(board, test);
    return test;
}

void move_a(vector<int>& board, bool& test){
    int i, j;
    for(j = 0; j < 3; j++){
        for(i = 0; i < 15; i++){
            if((i+1) % 4 == 0){
                i++;
            }
            if((board[i] == 0) && (board[i+1] != 0)){
                board[i] = board[i+1];
                board[i+1] = 0;
                test = true;
            }
        }
    }
}

bool d(vector<int>& board, int& score){
    int i;
    bool test = false;
    move_d(board, test);
    for(i = 15; i > 0; i--){
        if(i % 4 == 0){
            i--;
        }
        if((board[i] == board[i-1]) && (board[i] != 0)){
            board[i] = 2*board[i];
            board[i-1] = 0;
            score = score + board[i];
            test = true;
        }
    }
    move_d(board, test);
    return test;
}

void move_d(vector<int>& board, bool& test){
    int i, j;
    for(j = 0; j < 3; j++){
        for(i = 15; i > 0; i--){
            if(i % 4 == 0){
                i--;
            }
            if((board[i] == 0) && (board[i-1] != 0)){
                board[i] = board[i-1];
                board[i-1] = 0;
                test = true;
            }
        }
    }
}

void add_rand(vector<int>& board){
    bool loop = true;
    int r;
    while(loop == true){
        r = rand() % 16;
        if(board[r] == 0){
            board[r] = 2;
            loop = false;
        }
    }
}

bool game(const vector<int>& board){
    int i;
    if(board.size() > 16) return false;
    for(i = 0; i < 16; i++){
        if(board[i] == 0){
            return true;
        }
    }
    for(i = 0; i < 15; i++){
        if((i+1) % 4 == 0){
            i++;
        }
        if(board[i] == board[i+1]){
            return true;
        }
    }
    for(i = 0; i < 12; i++){
        if(board[i] == board[i+4]){
            return true;
        }
    }
    return false;
}

bool input_evaluation(vector<int>& board, char key, int& score){
    bool in;
    if(key == 'w'){
        in = w(board, score);
    }
    else if(key == 's'){
        in = s(board, score);
    }
    else if(key == 'a'){
        in = a(board, score);
    }
    else if(key == 'd'){
        in = d(board, score);
    }
    else if(key == 'q'){
        in = true;
        board.push_back(-1); // This tells the function game to finish the game.
    }
    else{
        in = false;
    }
    return in;
}

bool winner(const vector<int>& board, bool& win_condition){
    int i;
    if(win_condition == true) return false;
    for(i = 0; i < 16; i++){
        if(board[i] == 2048){
            win_condition = true;
            return true;
        }
    }
    return false;
}
