# README #

This version of 2048 was written as a coursework at Imperial College London. 
It was meant as a coding exercise, replicating the 2048 game written by Gabriele Ciruli:
https://github.com/gabrielecirulli/2048